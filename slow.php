<html>
<head>
    <title>Blackfire demo</title>
</head>
<body>

<?php

function slowButImportantFunction($input)
{
    for($i=0;$i<5000000;$i++) {}
    return $input;
}


function timesTable($number) 
{
    $table = [];
    $factor = slowButImportantFunction($number);
    for($i=1;$i<11;$i++) {
        $table[$i] = $i * $factor;
    }

    return $table;
}

function display($table)
{
    echo implode(",", $table);
}

echo "Ten times table:\n";
$table = timesTable(10);
display($table);

?>


</body>
</html>

